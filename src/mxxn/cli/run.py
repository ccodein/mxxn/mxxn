"""The run module of the cli."""
import uvicorn
from argparse import (
        _SubParsersAction, Namespace, ArgumentDefaultsHelpFormatter)


def run_handler(args: Namespace) -> None:
    """
    Handle the run command.

    Args:
        args: The argparse namespace.
    """
    uvicorn.config.LOGGING_CONFIG['formatters']['default']['fmt'] =\
        '%(levelname)-5.5s [%(name)s] %(message)s'

    print('Server started.\n')

    uvicorn.run(
        'mxxn:asgi',
        host=args.host,
        port=args.port,
        workers=args.workers,
        log_level=args.log_level)


def add_parser(parser: _SubParsersAction) -> None:
    """
    Add the parser to the given parser.

    Args:
        parser: The parser to add a new one to.
    """
    run_parser = parser.add_parser(
        'run',
        help='Run the application.',
        add_help=False,
        formatter_class=ArgumentDefaultsHelpFormatter)

    run_parser.add_argument(
        '--help', action='help', help='Show this help message and exit.')

    run_parser.add_argument(
        '--host',
        default='127.0.0.1',
        help='Bind the socket to this host.')

    run_parser.add_argument(
            '--port',
            default=8000,
            type=int,
            help='Bind the socket to this port.')

    run_parser.add_argument(
            '--workers',
            default=1,
            type=int,
            help='Number of worker processes.')

    run_parser.add_argument(
            '--log_level',
            default='warning',
            choices=['critical', 'error', 'warning', 'info', 'debug', 'trace'],
            help='The log level.')

    run_parser.set_defaults(func=run_handler)
