"""The dev module of the cli."""
from mxxn.env import Mxxn, MxnApp, Mxn, is_develop
import uvicorn
import subprocess
from os import environ
from pathlib import Path
from site import getsitepackages
from argparse import (
        _SubParsersAction, Namespace, ArgumentDefaultsHelpFormatter)
from mxxn.exceptions import cli as cli_ex


def is_editable(env_pkg: str) -> bool:
    """Is the package installed in editable mode."""
    for site_package in getsitepackages():
        if list(Path(site_package).glob(f'__editable__.{env_pkg}*')):
            return True

    return False


def virtual_env_path() -> Path | None:
    """
    Get the path of the virtual environment.

    Returns:
        The path of the environment. If it is not a virtual
        environment, then None is returned.
    """
    if 'VIRTUAL_ENV' in environ:
        return Path(environ['VIRTUAL_ENV'])

    return None


def env_pkg_path(env_pkg: str) -> Path:
    """
    Get the path of the environment package.

    Args:
        env_pkg: The name of the environment.

    Raises:
        mxxn.exceptions.env.PackageNotExistError: If the package
            does not exist.
    """
    match env_pkg:
        case 'mxxn':
            return Mxxn().path

        case 'mxnapp':
            return MxnApp().path

        case _:
            return Mxn(env_pkg).path


def env_pkg_editable_root(env_pkg: str) -> Path | None:
    """
    Get the root path of the editable environment package.

    Args:
        env_pkg: The name of the environment.

    Raises:
        mxxn.exceptions.env.PackageNotExistError: If the package
            does not exist.
    """
    if is_editable(env_pkg):
        path = env_pkg_path(env_pkg)

        if (path/'../pyproject.toml').is_file():
            return path/'..'
        elif (path/'../../pyproject.toml').is_file():
            return path/'../..'

    return None


def watch_handler(args: Namespace) -> None:
    """
    Handle the watch command.

    Args:
        args: The argparse namespace.

    Raises:
        mxxn.exceptions.env.PackageNotExistError: If the package
            does not exist.
    """
    uvicorn.config.LOGGING_CONFIG['formatters']['default']['fmt'] =\
        '%(levelname)-5.5s [%(name)s] %(message)s'

    source_path = env_pkg_path(args.env_pkg)
    print('Server started in watch mode.\n')

    print(virtual_env_path())

    # uvicorn.run(
    #     'mxxn:asgi',
    #     host=args.host,
    #     port=args.port,
    #     reload=True,
    #     reload_dirs=str(source_path),
    #     log_level=args.log_level)


def test_handler(args: Namespace) -> None:
    """
    Handle the test command.

    Args:
        args: The argparse namespace.

    Raises:
        mxxn.exceptions.cli.NoTestsPathInPackage: If the environment
            package does not have a tests path.
        mxxn.exceptions.cli.PackageNotEditable: If the evironment package
            is not editable.
        mxxn.exceptions.env.PackageNotExistError: If the package
            does not exist.
    """
    editable_root_path = env_pkg_editable_root(args.env_pkg)
    source_path = env_pkg_path(args.env_pkg)

    if editable_root_path:
        if (editable_root_path/'tests').is_dir():
            if args.coverage:
                subprocess.run([
                    'pytest',
                    '--cov',
                    source_path,
                    editable_root_path], cwd=editable_root_path)
            else:
                subprocess.run([
                    'pytest',
                    '-s',
                    '-k',
                    args.pattern], cwd=editable_root_path/'tests')
        else:
            raise cli_ex.NoTestsPathInPackage(
                f'The {args.env_pkg} package does not have a tests path.')
    else:
        raise cli_ex.PackageNotEditable(
            f'The {args.env_pkg} package does not have a tests path.')


def check_handler(args: Namespace) -> None:
    """
    Handle the check command.

    Args:
        args: The argparse namespace.

    Raises:
        mxxn.exceptions.cli.PackageNotEditable: If the evironment package
            is not editable.
        mxxn.exceptions.env.PackageNotExistError: If the package
            does not exist.
    """
    editable_root_path = env_pkg_editable_root(args.env_pkg)
    source_path = env_pkg_path(args.env_pkg)

    if editable_root_path:
        match args.tool:
            case 'flake8':
                subprocess.run(
                    ['flake8'], cwd=editable_root_path)

            case 'mypy':
                subprocess.run(['mypy', source_path], cwd=editable_root_path)

    else:
        raise cli_ex.PackageNotEditable(
            f'The {args.env_pkg} package does not have a tests path.')


def doc_handler(args: Namespace) -> None:
    """
    Handle the doc command.

    Args:
        args: The argparse namespace.

    Raises:
        mxxn.exceptions.cli.NoDocumentationInPackage: If the environment
            package does not have a documentation.
        mxxn.exceptions.cli.PackageNotEditable: If the evironment package
            is not editable.
        mxxn.exceptions.env.PackageNotExistError: If the package
            does not exist.
    """
    editable_root_path = env_pkg_editable_root(args.env_pkg)
    source_path = env_pkg_path(args.env_pkg)

    if editable_root_path:
        if (editable_root_path/'docs').is_dir() \
                and (editable_root_path/'docs/Makefile').is_file():
            subprocess.run([
                'make',
                'html'], cwd=editable_root_path/'docs')
        else:
            raise cli_ex.NoDocumentationInPackage(
                f'The {args.env_pkg} package does not have a documentation.')
    else:
        raise cli_ex.PackageNotEditable(
            f'The {args.env_pkg} package does not have a tests path.')


def add_parser(parser: _SubParsersAction) -> None:
    """
    Add the parser to the given parser.

    Args:
        parser: The parser to add a new one to.
    """
    if is_develop():
        dev_parser = parser.add_parser(
            'dev',
            help='The development options.',
            add_help=False,
            formatter_class=ArgumentDefaultsHelpFormatter)

        dev_parser.add_argument(
            '-h',
            '--help',
            action='help',
            help='Show this help message and exit.')

        dev_parser.set_defaults(func=lambda args: dev_parser.print_help())

        subparsers = dev_parser.add_subparsers()

        watch_parser = subparsers.add_parser(
            'watch',
            help='Start the application server and watch for changes in a '
            '"mxxn" environment package.',
            add_help=False,
            formatter_class=ArgumentDefaultsHelpFormatter)

        watch_parser.add_argument(
            '-h',
            '--help',
            action='help',
            help='Show this help message and exit.')

        watch_parser.add_argument(
            '--host',
            default='127.0.0.1',
            help='Bind the socket to this host.')

        watch_parser.add_argument(
                '--port',
                default=8000,
                type=int,
                help='Bind the socket to this port.')

        watch_parser.add_argument(
                '--log_level',
                default='warning',
                choices=[
                    'critical', 'error', 'warning', 'info', 'debug', 'trace'],
                help='The log level.')

        watch_parser.add_argument(
                'env_pkg',
                help='The name of the "mxxn" environment package to be watched'
                ' (e.g. mxxn, mxnapp).')

        watch_parser.set_defaults(func=watch_handler)

        test_parser = subparsers.add_parser(
            'test',
            help='Run the unit tests of a "mxxn" environment package.',
            add_help=False,
            formatter_class=ArgumentDefaultsHelpFormatter)

        test_parser.add_argument(
            '-h',
            '--help',
            action='help',
            help='Show this help message and exit.')

        test_parser.add_argument(
                'env_pkg',
                help='The name of the "mxxn" environment package to be watched'
                ' (e.g. mxxn, mxnapp).')

        test_parser.add_argument(
                '--pattern',
                default='test_',
                help='The file search pattern like the -k switch of pytest.')

        test_parser.add_argument(
                '--coverage',
                action='store_true',
                help='Print the coverage of a "mxxn" environment package.')

        test_parser.set_defaults(func=test_handler)

        check_parser = subparsers.add_parser(
            'check',
            help='Checks for a "mxxn" environment package.',
            add_help=False,
            formatter_class=ArgumentDefaultsHelpFormatter)

        check_parser.add_argument(
            '-h',
            '--help',
            action='help',
            help='Show this help message and exit.')

        check_parser.add_argument(
                'tool',
                choices=['flake8', 'mypy'],
                help='The tool to be used.')

        check_parser.add_argument(
                'env_pkg',
                help='The name of the "mxxn" environment package to be checked'
                ' (e.g. mxxn, mxnapp).')

        check_parser.set_defaults(func=check_handler)

        doc_parser = subparsers.add_parser(
            'doc',
            add_help=False,
            formatter_class=ArgumentDefaultsHelpFormatter,
            help='Generate the html documentation.')

        doc_parser.add_argument(
            '-h',
            '--help',
            action='help',
            help='Show this help message and exit.')

        doc_parser.add_argument(
                'env_pkg',
                help='The name of the "mxxn" environment package to be checked'
                ' (e.g. mxxn, mxnapp).')

        doc_parser.set_defaults(func=doc_handler)


# import asyncio
# import sys
# from subprocess import PIPE, STDOUT
#
#
# async def get_lines(shell_command):
#     p = await asyncio.create_subprocess_shell(
#         shell_command, stdin=PIPE, stdout=PIPE, stderr=STDOUT
#     )
#     return (await p.communicate())[0].splitlines()
#
#
# async def main():
#     # get commands output in parallel
#     coros = [
#         get_lines(
#             f'"{sys.executable}" -c "print({i:d}); import time; time.sleep({i:d})"'
#         )
#         for i in range(5)
#     ]
#     print(await asyncio.gather(*coros))
#
#
# if __name__ == "__main__":
#     asyncio.run(main())
