Settings
========

The settings can be passed via an TOML file or by environment variables.
The following sequence applies: environment variables overwrite the
default values and variables from the TOML file overwrite the data of
the environment variables.

Mxxn itself reads the *mxxn* sections variables of the settings file.
The Mxxn settings file can be passed to the framework via the environment
variable *MXXN_SETTINGS*. If the variable is not used, the settings.ini file
is searched for in the current working directory. If no settings file is
applied, the default settings of the Mxxn framework are used.

List of variables:

+--------------+----------------------+------+--------------------------+
| Variable     | Environment variable | Type | Description              |
+==============+======================+======+==========================+
| enabled_mxns | MXXN_ENABLED_MXNS    | list | List of enabled mxns     |
+--------------+----------------------+------+--------------------------+
| app_path     | MXXN_APP_PATH        | str  | Application directory    |
+--------------+----------------------+------+--------------------------+
| data_path    | MXXN_DATA_PATH       | str  | Data directory           |
+--------------+----------------------+------+--------------------------+
| database_url | MXXN_DATABASE_URL    | str  | SQLAlchemy database url  |
+--------------+----------------------+------+--------------------------+

**enabled_mxns**
  It is possible to activate only specific Mxns in the settings file.
  If the *enabled_mxns* variable of the settings file is not set,
  all installed Mxns will be activated. To deactivate all installed mxns,
  an empty list can be set in the settings file.

**app_path**         
  The application path is the location where the runtime data of the
  application are stored. This is usually where the settings.ini and
  the data folder are located, which contains, for example, the SQLite
  database, if used. If the app_path variable of the settings file is
  not set, the current working directory at the time of the application
  start will be used.

**data_path**
  The Data folder is normally located in the application path and
  contains, for example, the SQLite database, if one is used. The files
  folder, in which the uploaded files are stored, is also located there.
  If the data_path variable of the settings file is not set, the
  *app_path/data* will be used.

**sqllchemy_url**
  The SQLAlchemy database URL. If this was not set, the
  default URL *sqlite:///<data_path>/mxxn.db* will be used.
