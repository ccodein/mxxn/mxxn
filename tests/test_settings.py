"""Tests for the settings module."""
import pytest
from os import chdir, environ
from pathlib import Path
from falcon import asgi
from falcon.testing import TestClient as Client
from unittest.mock import patch
from mxxn.settings import Settings, SettingsMiddleware, _find_file, load
from mxxn.exceptions import settings as settings_ex
from mxxn.exceptions import filesys as filesys_ex


class TestFile():
    """Test for the static _file() function."""

    def test_no_environment_variable(self):
        """The environment variable does not exist."""
        assert not _find_file()

    def test_settings_in_current_dir(self, tmp_path):
        """The settings from current dir used."""
        settings_file = tmp_path/'settings.toml'
        settings_file.touch()
        chdir(tmp_path)

        assert _find_file() == tmp_path/'settings.toml'

    def test_settings_in_env_variable(self, tmp_path):
        """The settings file is in the environment variable."""
        settings_file = tmp_path/'settings.toml'
        settings_file.touch()

        with patch.dict(environ, {'MXXN_SETTINGS': str(settings_file)}):
            assert _find_file() == tmp_path/'settings.toml'

    def test_settings_file_is_not_a_file(self, tmp_path):
        """The settings file is in the environment variable is not a file."""
        settings_file = tmp_path

        with patch.dict(environ, {'MXXN_SETTINGS': str(settings_file)}):
            with pytest.raises(filesys_ex.FileNotExistError):
                _find_file()

    def test_with_relative_path(self, tmp_path):
        """The settings file path is ralative."""
        path_1 = tmp_path/'path_1'
        path_2 = tmp_path/'path_2'
        path_1.mkdir()
        path_2.mkdir()
        settings_file = path_1/'settings.toml'
        settings_file.touch()
        chdir(path_2)

        with patch.dict(
                environ, {'MXXN_SETTINGS': '../path_1/settings.toml'}):
            assert _find_file() == settings_file


class TestLoad():
    """Test for the static _load() method of the Settings class."""

    def test_validation_error(self, tmp_path):
        """A settings file validation error."""
        settings_file = tmp_path/'settings.ini'
        settings_file.write_text(
            """
            [mxxn]
            enabled_mxns = 1
            database_url = 'driver://user:pass@localhost/dbname'
            """
        )

        with patch.dict(environ, {'MXXN_SETTINGS': str(settings_file)}):
            with pytest.raises(settings_ex.SettingsFormatError):
                load()

    def test_no_mxxn_section(self, tmp_path):
        """The mxxn section does not exist in the INI file."""
        settings_file = tmp_path/'settings.ini'
        settings_file.write_text(
            """
            [mxxn]
            database_url = 'driver://user:pass@localhost/dbname'
            """
        )

        with patch.dict(environ, {'MXXN_SETTINGS': str(settings_file)}):
            settings = load()

            assert settings.database_url ==\
                'driver://user:pass@localhost/dbname'

    def test_no_sections_in_settings_file(self, tmp_path):
        """It is no section in the settings file."""
        settings_file = tmp_path/'settings.ini'
        settings_file.write_text(
            """
            ggg
            """
        )

        with patch.dict(environ, {'MXXN_SETTINGS': str(settings_file)}):
            with pytest.raises(settings_ex.SettingsFormatError):
                load()

    def test_additional_property(self, tmp_path):
        """An addional property in the mxxn section."""
        settings_file = tmp_path/'settings.ini'
        settings_file.write_text(
            """
            [mxxn]
            additional = 123
            """
        )

        with patch.dict(environ, {'MXXN_SETTINGS': str(settings_file)}):
            with pytest.raises(settings_ex.SettingsFormatError):
                load()

    def test_settings_file_passed(self, tmp_path):
        """An existing settings file was passed."""
        settings_file = tmp_path/'settings.ini'
        settings_file.write_text(
            """
            [mxxn]
            database_url = 'driver://user:pass@localhost/dbname'
            """
        )

        settings = load(settings_file)

        assert settings.app_path == Path.cwd()
        assert settings.data_path == Path.cwd()/'data'
        assert not settings.enabled_mxns
        assert settings.database_url ==\
            'driver://user:pass@localhost/dbname'

    def test_passed_file_not_exist(self, tmp_path):
        """The passed file does not exist."""
        with pytest.raises(filesys_ex.FileNotExistError):
            load(Path('sdsdskddfdsfksd/dsdsdssd/sdsds'))

    def test_enabled_mxns_not_list(self, tmp_path):
        """The enabled_mxns variable is not a list."""
        settings_file = tmp_path/'settings.ini'
        settings_file.write_text(
            """
            [mxxn]
            enabled_mxns = dsds s 1
            """
        )

        with pytest.raises(settings_ex.SettingsFormatError):
            load(settings_file)


class TestSettingsDefaults():
    """Test for the defaults of the Settings class."""

    def test_no_args_passed(self):
        """No args passed."""
        settings = Settings()

        assert settings.app_path == Path.cwd()
        assert settings.data_path == Path.cwd()/'data'
        assert not settings.enabled_mxns
        assert settings.database_url ==\
            f'sqlite+aiosqlite://{settings.data_path}/mxxn.db'

    def test_app_path_passed(self, tmp_path):
        """A app_path passed."""
        settings = Settings(app_path=tmp_path)

        assert settings.app_path == tmp_path
        assert settings.data_path == tmp_path/'data'
        assert not settings.enabled_mxns
        assert settings.database_url ==\
            f'sqlite+aiosqlite://{settings.data_path}/mxxn.db'

    def test_data_path_passed(self):
        """A data_path passed."""
        settings = Settings(data_path='/data/path')

        assert settings.app_path == Path.cwd()
        assert settings.data_path == Path('/data/path')
        assert not settings.enabled_mxns
        assert settings.database_url ==\
            'sqlite+aiosqlite:///data/path/mxxn.db'

    def test_database_url_passed(self):
        """A database_url passed."""
        settings = Settings(
            database_url='postgresql+asyncpg://scott:tiger@localhost:5432/db'
        )

        assert settings.app_path == Path.cwd()
        assert settings.data_path == Path.cwd()/'data'
        assert not settings.enabled_mxns
        assert settings.database_url ==\
            'postgresql+asyncpg://scott:tiger@localhost:5432/db'


class TestSettingsEnvironmentVariable():
    """Test for the defaults of the Settings class."""

    def test_app_path_env(self, tmp_path):
        """A app_path in the environment variables."""
        with patch.dict(environ, {'MXXN_APP_PATH': str(tmp_path)}):
            settings = Settings()

        assert settings.app_path == tmp_path

    def test_data_path_env(self, tmp_path):
        """A data_path in the environment variables."""
        with patch.dict(environ, {'MXXN_DATA_PATH': str(tmp_path)}):
            settings = Settings()

        assert settings.data_path == tmp_path

    def test_enabled_mxns_env(self, tmp_path):
        """A enabled_mxns in the environment variables."""
        with patch.dict(environ, {'MXXN_ENABLED_MXNS': '["one", "two"]'}):
            settings = Settings()

        assert settings.enabled_mxns == ['one', 'two']

    def test_database_url_env(self, tmp_path):
        """A enabled_mxns in the environment variables."""
        url = 'postgresql+asyncpg://scott:tiger@localhost:5432/db'

        with patch.dict(environ, {'MXXN_DATABASE_URL': url}):
            settings = Settings()

        assert settings.database_url == url


class TestSettingsMiddleware():
    """Tests for the SettingsMiddleware class."""

    def test_settings_added_to_context(self):
        """The settings object was added to req.context."""
        class Resource():
            async def on_get(self, req, resp):
                req.context.settings

        settings = Settings()
        settings_middleware = SettingsMiddleware(settings)
        app = asgi.App()
        app.add_middleware(settings_middleware)
        app.add_route('/', Resource())

        client = Client(app)
        resp = client.simulate_get('/')

        assert resp.status_code == 200
